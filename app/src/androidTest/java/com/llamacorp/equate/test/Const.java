package com.llamacorp.equate.test;


public class Const {
	public static final int TEMP = 1;
	public static final int WEIGHT = 2;
	public static final int LENGTH = 3;
	public static final int VOLUME = 4;
	public static final int SPEED = 5;
	
	public static final int F = 4;
	public static final int K = 8;
	public static final int C = 9;
	
	public static final int IN = 0;
	public static final int FT = 1;
	public static final int YARD = 2;
	public static final int MILE = 3;
	public static final int KM = 4;
	public static final int UM = 5;
	public static final int MM = 6;
	public static final int CM = 7;
	public static final int M = 8;
	
}
