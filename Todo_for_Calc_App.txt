------------------ToDo Next--------------------
- Should perhaps standardized date styling using pure HTML (instead of extra textView)
- When doing regular currency to historical, should change to year date code (instead of day or hour)
- Should use API update historical USD to today's USD
- Remove timeout not reached toast

-Add rpm and rad/sec

- Incorporate in yahoo's finance data http://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote
Www.xe.com/symbols.php
- 2015 Historical (grab latest CPI numbers)
- Click equals again for sci notation - when going back into reg, grab more accurate version (store it in a precise BigDecimal

&#2261;


- getting occasional null pointer from "getListView().getPositionForView((View)view.getParent());" in ResultListFrag
- Error from getListView().setSelection(getListAdapter().getCount()-1); content view not created yet
- When currency update, toast
- Make secondary buttons text change to black when clicked
- Think about adding a graphic for shrinking unit keys
- Make convert summary text view's font size slightly dynamic
- Add square root
http://stackoverflow.com/questions/1309629/how-to-change-colors-of-a-drawable-in-android

-----Stuff to Test (squashed bugs, etc)-----
- Empty expression, tap previous result with different unit type than curr, adds a one
- Test min/mile to mph and back again
- Reset then hit num and then unit, unit doesn't appear
- Make sure tapping "Convert" doesn't select text or break, also try entering text after that
- Dialog boxes for first unit conversion 
- When syntax error thrown make sure unit is not displayed, aka not "Syntax Error in"
- 1E99999 should just throw number too large error
- Select input it in weight, hit back, come back, still selected. 
- Select key at left and right most convkeys and swipe away, make sure key is not selected any more
- Select key at left+1 and right-1 convkey pos and move to left and right respectively, make sure no key is selected
- Hold click empty expression brings up menu (dunno how to test this...)
- Hold click full expression selects all text and brings up menu
- When app is closed (or whatever backspace does) make sure UnitType pos and Unit selection are preserved
- When unit type is switched, set cursor pos to end
- Be sure convert that are empty don't break things
- Hold click on empty expression should pop up paste dialog (maybe clicking or down press brings up cursor?)
- Check all unit values
- ResultList has a max size
- Catch JSON read errors and do a reset calc
- When clicking result with unit not visible, unit doesn't stay selected

----------------------Fix----------------------
---- TRY TO ADD UNIT TESTING FOR ALL PROBLEMS FIXED -----
- Convert unit to another, tap expression, adds convert to
- After long press unit switch, select unit
- Type "54+43)" and delete auto close para --> syntax error
- Screen rotation clears unit
- For every solve or unit convert, save app state
- Font size issues with large text sizes (also run the emulator for a few diff size screens and resolutions. 
- When prev expression exists, operators should take last expression it back instead of doing nothing even when current expression is empty 
- Black flash seems to happen after clear operation?
- Construct results out of expressions such that 1/3, then delete, then recall 0.33333*3 == 1
- "Converting inches to feet" bug still there; do unit coverts, back, then open app again
- (fixed?) If its been a while since open, app crashes
- Reduce redundant case checking before an big number addition/subtraction
- Backspace drawable selected turns white instead of dark
- Single character vs multi character results are treated differently when clicked with Unit
- Use of AndroidManifest file to change styles seems crude, investigate this
- Perhaps selection access should be revisited--should it be initiated from the model side or the controller side?

-------------------Features----------------------
- Maybe add a [Exact] or [Approx] to each conversion
- Add check/click results functionality to ConsoleTester
- Add sqrt
- Add fractional mode 
- Tap equals after solve for scientific notation (hold equals for sci notation preferences)?
- Add preferences 
- Make "Convert milimeters to..." blink or something cool
- Make arrows in a circle instead of line
- Make arrows pulse when waiting to convert
- Also allow for custom unit
- Implement infinity properly instead of just saying number too large
- Kitkat now has arrows at bottom. To move cursor, add these to calc
- Maybe add sqrt and possibly trig
- Try to implement pretty print, or at least superscript ^'s
- Add phantom close paren after open? Goes away with any selection, close/equals makes permanent, 
- Pinch zoom to change size of prev expression's text
- Re-factor and clean code (make an expression class)
- Use gridlayout and support library instead of nested weights and linear layouts
- Copy and maybe paste buttons (maybe left side of expression's text box) show copy button after solve and paste if properly formatted thing in clipboard?
- Maybe copy and paste in status bar, although this is going to take a lot of vertical space
- Add hold options for lots of keys (add indicating names for these) eg, sqrt, !, mod
- Explore adding another coloum of keys to main calc app, might be too cluttered, but also might be nice (maybe make it an option?)
- Swipe down on units to hide; use three horz lines to signify swipeable. When hidden only show unittypes 
- Use animation to move expression up and to the left after ='s; also make answer bold or something for a second
- Use spannable to create custom colors for operators and numbers 
- Swipe out left from numbers to access options menu (flip EE/^, maybe custom units, default units open or closed)
- Add programming conversions (binary, hex, oct etc)

----Usability Upgrades----
- For first time opening show how the units can be slide left and right and other not obvious features


- Change event flow (1. User input in controller, 2. Controller calls model with basic user input, model updates some stuff maybe 3. Controller updates screen liberally) model classes should be usable in iOS app

Why singleton:
- Just get this done, make it cleaner when it's finished.
- What are you really loosing this way?
- The book did it
- Where would calc live otherwise (in the activity seems like a dumb place for it)
- How would fragments access it easily (passing between frag to activity seems uncessarily difficult)
- This also somewhat solves saving issues